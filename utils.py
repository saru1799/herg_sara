import pandas as pd
from sklearn.metrics import roc_auc_score
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.preprocessing import LabelEncoder, OneHotEncoder


def challenge_encoding(X_train, X_ch):
    
    """
    Description
    -----------
    Do a categorical encoding
    
    Arguments
    ----------
    X_train (numpy array): training data with which we train the model.
    They are needed as a basis in the categorical encoding. 
    Can be the output of sklearn.train_test_split() function.
    
    X_ch (numpy array): data from which we want to predict its label.
        
    Returns
    ----------
    X_ch_enc (numpy array): encoded codified numpy array that models need.
    X_ch_df (DataFrame): encoded codified df. 
    
    """

    # Convert numpy arrays in df
    c = ['affected_domain', 'initial_aa', 'final_aa', 'functional_domain',
         'd_size', 'd_hf', 'd_vol', 'd_msa', 'd_charge', 'd_pol', 'd_aro', 'residue_conserv', 'secondary_str']
    
    X_train_df = pd.DataFrame(X_train, columns = c)
    X_ch_df = pd.DataFrame(X_ch, columns = c)
    
    l = ["affected_domain","initial_aa", "final_aa", 'functional_domain',
         "d_charge", "d_pol", "d_aro", "secondary_str"]
    
    for i in l: 
        # First: LabelEncoder()
        le = LabelEncoder()
        le.fit(X_train_df[i]) # fit only in training set
        domain_labels_train = le.transform(X_train_df[i])
        domain_labels_ch = le.transform(X_ch_df[i])
        X_train_df["label_train"] = domain_labels_train
        X_ch_df["label_ch"] = domain_labels_ch

        # Second: OneHotEncoder
        oe = OneHotEncoder()
        oe.fit(X_train_df[[i]])
        domain_feature_arr_train = oe.transform(X_train_df[[i]]).toarray()
        domain_feature_arr_ch = oe.transform(X_ch_df[[i]]).toarray()

        domain_feature_labels = list(le.classes_)
        domain_features_train = pd.DataFrame(domain_feature_arr_train, columns = domain_feature_labels)
        domain_features_ch = pd.DataFrame(domain_feature_arr_ch, columns = domain_feature_labels)


        # Update df
        X_ch_df = pd.concat([X_ch_df, domain_features_ch], axis = 1)
        X_ch_df = X_ch_df.drop(["label_ch",i], axis =1)

    return X_ch_df.to_numpy(), X_ch_df
    
    
########################################################################################################################

    
def categorical_encoding(X_train, X_test):
    """
    Description
    -----------
    Do a categorical encoding
    
    Arguments
    ----------
    X_train (numpy array): training data with which we train the model.
    They are needed as a basis in the categorical encoding. 
    Can be the output of sklearn.train_test_split() function.
    
    X_test (numpy array): data from which we want to predict its label.
    Can be the output of sklearn.train_test_split() function.
        
    Returns
    ----------
    X_train_enc (numpy array): encoded codified numpy array that models need.
    X_train_df (DataFrame): encoded codified df. 
    X_test_enc (numpy array): encoded codified numpy array that models need.
    X_test_df (DataFrame): encoded codified df. 
    
    """

    # Convert numpy arrays in df
    c = ['affected_domain', 'initial_aa', 'final_aa', 'functional_domain','d_mw', 'd_hf', 'd_vol', 
         'd_msa', 'd_charge', 'd_pol', 'd_aro', 'residue_conserv', 'secondary_str', 'alphaf_sec']
    
    X_train_df = pd.DataFrame(X_train, columns = c)
    X_test_df = pd.DataFrame(X_test, columns = c)
    
    l = ["affected_domain","initial_aa", "final_aa", 'functional_domain',
         "d_charge", "d_pol", "d_aro", "secondary_str"]
    
    for i in l: 
        # First: LabelEncoder()
        le = LabelEncoder()
        le.fit(X_train_df[i]) # fit only in training set
        domain_labels_train = le.transform(X_train_df[i])
        domain_labels_test = le.transform(X_test_df[i])
        X_train_df["label_train"] = domain_labels_train
        X_test_df["label_test"] = domain_labels_test

        # Second: OneHotEncoder
        oe = OneHotEncoder()
        oe.fit(X_train_df[[i]])
        domain_feature_arr_train = oe.transform(X_train_df[[i]]).toarray()
        domain_feature_arr_test = oe.transform(X_test_df[[i]]).toarray()
        domain_feature_labels = list(le.classes_)
        domain_features_train = pd.DataFrame(domain_feature_arr_train, columns = domain_feature_labels)
        domain_features_test = pd.DataFrame(domain_feature_arr_test, columns = domain_feature_labels)

        # Update df
        X_train_df = pd.concat([X_train_df, domain_features_train], axis = 1)
        X_train_df = X_train_df.drop(["label_train",i], axis =1)
        
        X_test_df = pd.concat([X_test_df, domain_features_test], axis = 1)
        X_test_df = X_test_df.drop(["label_test",i], axis =1)

    
    return X_train_df.to_numpy(), X_test_df.to_numpy(), X_train_df, X_test_df 


########################################################################################################################
  
    
def get_roc_auc_score(model, X_tr, y_tr, X_te, y_te, print_table = True):
    """
  Arguments
  ----------
  model: model to use.
  X_tr: training set.
  y_tr: labels of training set.
  X_te: test set.
  y_te: labels of test set.
  print_table=True (default)
  Return
  ---------
  Returns a table with roc-auc 
  calculation for training and test sets.
    """
    
    y_tr_p = model.predict(X_tr)
    y_te_p = model.predict(X_te)
  
    er_tr = [roc_auc_score(y_tr, y_tr_p)]
    er_te = [roc_auc_score(y_te, y_te_p)]
    
    ers = [er_tr, er_te]
    headers=["Roc-auc"]
    
    if print_table:
        print("%10s" % "", end="")
    
        for h in headers:
            print("%10s" % h, end="")
        print("")
    
        headersc = ["Train", "Test"]
    
        cnt = 0
        for er in ers:
            hc = headersc[cnt]
            cnt = cnt + 1
            print("%10s" % hc, end="")
    
            for e in er:
                print("%10.2f" % e, end="")
            print("")
    
    return ers


########################################################################################################################

    
def select_features_RF(X_train, X_train_df, y_train, X_test, k='all'):
    """
  Arguments
  ----------
  X_train: training set.
  y_train: labels of training set.
  X_test: test set.
  Return
  ---------
  Apply best FS for RF
    """
    l_feat = []
    l_pos = []
    fs = SelectKBest(score_func=f_classif, k=k)
    fs.fit(X_train, y_train)
    if k != 'all':

        index = fs.fit(X_train, y_train).get_support(indices = True)
        l_pos.append(index)
        l_feat.append(X_train_df.columns[:][index])

    X_train_fs = fs.transform(X_train)
    X_test_fs = fs.transform(X_test)
    return X_train_fs, X_test_fs