Este repositorio contiene la base de datos empleada en el estudio del canal hERG (MUT_Final.csv).
Se añaden también el algoritmo empleado para la predicción de la patogenicidad. Ejecutarlo mostraá los valores de la especificidad, sensibilidad y ROC-AUC.

TFG Sara Navarro 2022.
