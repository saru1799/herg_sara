import utils
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, plot_confusion_matrix







# Leemos la base de datos

wdf = pd.read_csv('MUT_FINAL.csv', header = "infer")

X = wdf.values[:,2:-1]
y = wdf.values[:,-1].astype('int')

# Separación de la base de datos: train y test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=12)

#categorcal encoding
X_train_enc, X_test_enc, X_train_df, X_test_df = utils.categorical_encoding(X_train, X_test)


# ""Feature selection"" para el algoritmo (k = "all")
X_train_fs, X_test_fs = utils.select_features_RF(X_train_enc, X_train_df, y_train, X_test_enc)


print("El número de mutaciones en el training set es de:")
ben_tot_train = np.unique(y_train, return_counts=True)[1][0]
pat_tot_train = np.unique(y_train, return_counts=True)[1][1]
print("· {} benignas".format(ben_tot_train))
print("· {} patológicas".format(pat_tot_train))

print()
ben_tot_test = np.unique(y_test, return_counts=True)[1][0]
pat_tot_test = np.unique(y_test, return_counts=True)[1][1]
print("El número de mutaciones en el test set es de:")
print("· {} benignas".format(ben_tot_test))
print("· {} patológicas".format(pat_tot_test))

# HIPERPARÁMETROS ELEGIDOS

crit='gini'
min_ss = 4
max_feat = 'log2'
boots = True
m_samp = None
class_w= {0: 10, 1: 2}
n_esti = 110
max_d = 2




# Definición del Pipeline
pipeline_RF = Pipeline( [("scaler", StandardScaler()), \
                        ("RF", RandomForestClassifier( criterion = crit,
                                                        max_depth = max_d,
                                                        min_samples_split = min_ss,
                                                        n_estimators = n_esti,
                                                        max_features = max_feat,
                                                        bootstrap = boots,
                                                        max_samples = m_samp,
                                                        class_weight = class_w,
                                                        random_state = 5))] )
 # Entrenamiento -> fit
pipeline_RF.fit(X_train_fs, y_train)

roc_auc =utils.get_roc_auc_score(pipeline_RF, X_train_fs, y_train, 
                        X_test_fs, y_test, print_table=True)

# Se crea la matriz de confusión (PREDICCIÓN PARA EL TRAIN)
RF_prediction = pipeline_RF.predict(X_train_enc)
a = confusion_matrix(y_train, RF_prediction)

ben = (a[0][0] / 
(a[0][0]+
        a[0][1])) * 100
true_ben = a[0][0]
pat = (a[1][1] / 
(a[1][0]+
        a[1][1])) * 100
true_pat = a[1][1]





# Se crea la matriz de confusión (PREDICCIÓN PARA EL TEST)

RF_prediction = pipeline_RF.predict(X_test_enc)
b = confusion_matrix(y_test, RF_prediction)


ben2 = (b[0][0] / 
        (b[0][0]+
        b[0][1])) * 100
true_ben2 = b[0][0]
pat2 = (b[1][1] / 
        (b[1][0]+
        b[1][1])) * 100
true_pat2 = b[1][1]

# Matrices de confusión para train y test
# confussion train

plot_confusion_matrix(pipeline_RF, X_train_enc, y_train)
plt.savefig('train_confussion_SARA.png')
# confussion test

plot_confusion_matrix(pipeline_RF, X_test_enc, y_test)
plt.savefig('test_confussion_SARA.png')

# Imprimir valores de ROC-AUC, especificidad y sensibilidad

print('ROC-AUC:', roc_auc)

print("Train:")
print("· ESPECIFICIDAD: {} % de las mutaciones benignas ({} de {} mutaciones benignas).".format(ben,true_ben, ben_tot_train))
print("· SENSIBILIDAD: {} % de las mutaciones patológicas ({} de {} mutaciones patológicas).".format(pat,true_pat, pat_tot_train))
print()

print("Test: ")
print("· ESPECIFICIDAD: {} % de las mutaciones benignas ({} de {} mutaciones benignas).".format(ben2,true_ben2, ben_tot_test))
print("· SENSIBILIDAD: {} % de las mutaciones patológicas ({} de {} mutaciones patológicas).".format(pat2,true_pat2, pat_tot_test))


c = X_train_df.columns


importances = pd.DataFrame({'Característica':c,'Importancia':np.round(pipeline_RF.named_steps["RF"].feature_importances_,3)})
importances = importances.sort_values('Importancia',ascending=False).set_index('Característica')

## Obtención de la importancia
## Clean features with 0 importance
# Get names of indexes for which column importances has value 0.000
indexNames = importances[ importances['Importancia'] <= 0.01].index
# Delete these row indexes from dataFrame

relevant_importances = importances.drop(indexNames , inplace=True)
importances.plot.bar()

# plt.show() # Unncomment para sacar la imagen

